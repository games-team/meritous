meritous (1.5-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Drop transition for old debug package migration.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 23 Oct 2022 08:36:20 -0000

meritous (1.5-1) unstable; urgency=medium

  * New upstream release merging Debian additions
  * Revamp packaging

 -- Sylvain Beucler <beuc@debian.org>  Wed, 27 Feb 2019 00:12:56 +0100

meritous (1.4-4) unstable; urgency=medium

  * Include scalable icon from upstream (fix appstream icon-too-small and
    gui-app-without-icon)
  * Preserve dpkg-buildflags' CPPFLAGS (fix dpkg-buildflags-missing)

 -- Sylvain Beucler <beuc@debian.org>  Sat, 02 Feb 2019 23:27:27 +0100

meritous (1.4-3) unstable; urgency=medium

  * Fix dbgsym-migration target package
  * Install AppStream file

 -- Sylvain Beucler <beuc@debian.org>  Sun, 27 Jan 2019 00:38:12 +0100

meritous (1.4-2) unstable; urgency=medium

  * Update upstream and Debian URLs
  * Update obsolete 'extra' priority to 'optional'
  * Bump compat to 12 (hopefully helping dpkg-buildflags-missing)
  * Bump Standard to 4.3.0 (no changes)
  * Use .png icon instead of .xpm
  * Remove old Debian menu entries
  * Ease potential cross-arch dependencies with Multi-Arch:foreign
  * Migrate debug symbols from manual -dbg to automated -dbgsym
  * Remove 'quilt' build-dependency, now built-in in dpkg
  * Update README.source

 -- Sylvain Beucler <beuc@debian.org>  Sat, 26 Jan 2019 22:50:53 +0100

meritous (1.4-1) unstable; urgency=low

  * New upstream release
  * Doc fixes
  * Bump Standard to 3.9.5 (no changes)
  * Fix lintian binary-control-field-duplicates-source and
    vcs-field-not-canonical

 -- Sylvain Beucler <beuc@debian.org>  Thu, 28 Nov 2013 19:13:17 +0100

meritous (1.3-1) unstable; urgency=low

  * New upstream release - all patches merged, non-free files removed

 -- Sylvain Beucler <beuc@debian.org>  Sat, 09 Nov 2013 12:15:49 +0100

meritous (1.2+dfsg-3) unstable; urgency=low

  * Reupload to unstable
  * Bump compat to 9 and patch Makefile to address lintian 'hardening-no-relro'
  * Bump Standard to 3.9.4

 -- Sylvain Beucler <beuc@debian.org>  Sun, 13 Oct 2013 17:46:50 +0200

meritous (1.2+dfsg-2) experimental; urgency=low

  * Reupload to Debian and take over (Closes: #696582)
  * Use debhelper 7
  * Use 3.0 source format
  * Add -dbg package
  * Add zlibg1-dev to Build-Depends, cf. #669571 (Closes: #669439)
  * Add note in debian/copyright to explain dfsg changes
  * Update path to manpages/docbook.xsl
  * Fix typo in description

 -- Sylvain Beucler <beuc@debian.org>  Sun, 23 Dec 2012 12:39:15 +0100

meritous (1.2+dfsg-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/patches/undefined-reference.patch: Fixed FTBFS.  Closes: #555597.
  * Fixed debian-watch-file-should-mangle-version.
  * Fixed dh_desktop-is-deprecated.

 -- Bart Martens <bartm@debian.org>  Tue, 04 Oct 2011 19:48:07 +0200

meritous (1.2+dfsg-1) unstable; urgency=low

  * Dylan R. E. Moonfire
    + src/levelblit.c: Updated the version to reflect the actual version
      instead of "1.1".
    + debian/control: Added Vcs- headers to point to Subversion
      repository.
    + Changed the location of the save game to use
      $HOME/.meritous.sav. This will use legacy save file if the new one
      cannot be found. (Closes: #465228)
    + src/help.c: Corrected a parsing bug with the help files that
      prevented users from reading the help files. (Closes: #465147)
    + Removed music files due to licensing issues. Patched code to make
      music files optional. (Closes: #465532)
    + debian/rules: Added a 'get-orig-source' target to repackage the
      original after removing the music files.
  * Bas Wijnen
    + src/boss.c: Applied a patch to handle an array overflow in the first
      boss fight. (Closes: #465051)

 -- Dylan R. E. Moonfire <debian@mfgames.com>  Mon, 11 Feb 2008 13:45:02 -0600

meritous (1.2-1) unstable; urgency=low

  * Initial release (Closes: #461102)
  * debian/patches/data_in_share.patch: Added patch to refer to data files
    into /usr/share/games/meritous.

 -- Dylan R. E. Moonfire <debian@mfgames.com>  Tue, 05 Feb 2008 22:14:53 -0600
